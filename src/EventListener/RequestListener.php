<?php

namespace App\EventListener;

use App\Utils\RequestLog\RequestLogWriter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

//listen events on request/response
class RequestListener
{
    private $writer;            //log writer logic

    public function __construct(EntityManagerInterface $em) {
        $this->writer = new RequestLogWriter($em);
    }

    //request event
    public function onKernelRequest(RequestEvent $event)
    {
        $this->writer->setRequestData($event);
    }

    //response event
    public function onKernelResponse(ResponseEvent $event)
    {
        if (HttpKernelInterface::MASTER_REQUEST != $event->getRequestType()) {
            return;
        }

        $this->writer->setResponseData($event);
        $this->writer->log();
    }

}
?>