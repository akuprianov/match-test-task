<?php

namespace App\Repository;

use App\Entity\RequestLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\Query;

/**
 * @method RequestLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method RequestLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method RequestLog[]    findAll()
 * @method RequestLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RequestLogRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RequestLog::class);
    }


    public function findPaginatedByIP($ip, $limit, $offset)
    {
        $qb = $this->createQueryBuilder('s')->orderBy('s.time', 'DESC');

        if ($ip)
            $qb->where('s.IP = :IP')->setParameter('IP', $ip);

        $qb->setMaxResults($limit)->setFirstResult($offset);

        //по хорошему сделать через SQL_CALC_FOUND_ROWS
        $paginator = new Paginator($qb);

        return [
          'count'   => count($paginator),
          'limit'   => $limit,
          'results' =>$qb->getQuery()->getResult(Query::HYDRATE_ARRAY)
        ];
    }
}
