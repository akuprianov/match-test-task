<?

namespace App\Utils\RequestLog;

use App\Entity\RequestLog;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

//class log request
class RequestLogWriter
{
    private $em;                //entity manager

    private $save_log = false;  //flag of save log
    private $uri;               //url
    private $request_headers;   //request headers
    private $request_content;   //request body
    private $response_headers;  //response headers
    private $response_content;  //response body
    private $response_code;     //response code
    private $method;            //response method
    private $IP;                //visitor IP


    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    //get request data
    public function setRequestData(RequestEvent $event)
    {
        $headers        = $event->getRequest()->headers->all();
        $this->save_log = !empty($event->getRequest()->get('save_log'));

        $this->method  = $event->getRequest()->getMethod();
        $this->uri     = $event->getRequest()->getUri();
        $this->IP      = $event->getRequest()->getClientIp();
        $this->request_content = $event->getRequest()->getContent();
        $this->request_headers = $this->headersToText($headers);
    }

    //get response data
    public function setResponseData(ResponseEvent $event)
    {
        $headers = $event->getResponse()->headers->all();

        $this->response_code    = $event->getResponse()->getStatusCode();
        $this->response_headers = $this->headersToText($headers);
        $this->response_content = $event->getResponse()->getContent();
    }

    //save data log
    public function log()
    {
        if (!empty($this->save_log))
        {
            try {
                $log = (new RequestLog())
                  ->setUri($this->uri)
                  ->setRequestHeaders($this->request_headers)
                  ->setRequestContent($this->request_content)
                  ->setResponseHeaders($this->response_headers)
                  ->setResponseContent($this->response_content)
                  ->setResponseCode($this->response_code)
                  ->setIP($this->IP)
                  ->setMethod($this->method)
                  ->setTime(time());

                $this->em->persist($log);
                $this->em->flush();
            } catch (\Exception $e) {
                //some logic for exception
                //$error = __CLASS__." error: " . $e->getMessage();
            }
        }
    }

    //convert headers array to text
    private function headersToText($headers)
    {
        $result = '';

        foreach ($headers as $name => $values)
            foreach($values as $val)
                $result .= ucwords($name).": ".$val."\r\n";

        return trim($result);
    }

}

?>