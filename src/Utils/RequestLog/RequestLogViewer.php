<?

namespace App\Utils\RequestLog;

use App\Entity\RequestLog;
use App\Utils\PagerTrait;
use Doctrine\Common\Persistence\ObjectManager;

//class to view log request
class RequestLogViewer
{
    use PagerTrait;

    private $em;         //entity manager
    private $limit = 10; //limit of results

    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }

    //get request data
    public function getData(int $page, string $ip = '') : array
    {
        try {
            $page   = $this->getPage($page);
            $limit  = $this->getLimit($this->limit);
            $offset = $this->getOffset($page, $limit);

            $log = $this->em->getRepository(RequestLog::class)->findPaginatedByIP($ip, $limit, $offset);
        } catch (\Exception $e) {
            throw $e;
        }

        return $log;
    }
}

?>