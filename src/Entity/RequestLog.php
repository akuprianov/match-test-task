<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="request_logs", indexes={
 *      @ORM\Index(name="IP_index", columns={"IP"}),
 *      @ORM\Index(name="time_index", columns={"time"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\RequestLogRepository")
 */
class RequestLog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $uri;

    /**
     * @ORM\Column(type="text")
     */
    private $request_headers;

    /**
     * @ORM\Column(type="text")
     */
    private $request_content;

    /**
     * @ORM\Column(type="text")
     */
    private $response_headers;

    /**
     * @ORM\Column(type="text")
     */
    private $response_content;

    /**
     * @ORM\Column(type="integer")
     */
    private $response_code;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $IP;

    /**
     * @ORM\Column(type="string", length=16)
     */
    private $method;

    /**
     * @ORM\Column(type="integer")
     */
    private $time;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUri(): ?string
    {
        return $this->uri;
    }

    public function setUri(string $uri): self
    {
        $this->uri = $uri;

        return $this;
    }

    public function getRequestHeaders(): ?string
    {
        return $this->request_headers;
    }

    public function setRequestHeaders(string $request_headers): self
    {
        $this->request_headers = $request_headers;

        return $this;
    }

    public function getRequestContent(): ?string
    {
        return $this->request_content;
    }

    public function setRequestContent(string $request_content): self
    {
        $this->request_content = $request_content;

        return $this;
    }

    public function getResponseHeaders(): ?string
    {
        return $this->response_headers;
    }

    public function setResponseHeaders(string $response_headers): self
    {
        $this->response_headers = $response_headers;

        return $this;
    }

    public function getResponseContent(): ?string
    {
        return $this->response_content;
    }

    public function setResponseContent(string $response_content): self
    {
        $this->response_content = $response_content;

        return $this;
    }

    public function getResponseCode(): ?int
    {
        return $this->response_code;
    }

    public function setResponseCode(int $response_code): self
    {
        $this->response_code = $response_code;

        return $this;
    }

    public function getIP(): ?string
    {
        return $this->IP;
    }

    public function setIP(string $IP): self
    {
        $this->IP = $IP;

        return $this;
    }

    public function getMethod(): ?string
    {
        return $this->method;
    }

    public function setMethod(string $method): self
    {
        $this->method = $method;

        return $this;
    }

    public function getTime(): ?int
    {
        return $this->time;
    }

    public function setTime(int $time): self
    {
        $this->time = $time;

        return $this;
    }
}
