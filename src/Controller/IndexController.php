<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->render('/index/index.html.twig', [
            'document' => [
                'title' => 'Тестовое задание',
            ]
        ]);
    }

    /**
     * @Route("/test", name="test")
     */
    public function test()
    {
        return $this->json(['status' => true, 'rand' => rand(0, 1000)]);
    }


}
