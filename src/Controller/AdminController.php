<?php

namespace App\Controller;

use App\Utils\RequestLog\RequestLogViewer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin/http-log", name="admin")
     */
    public function index(Request $request)
    {
        $em   = $this->getDoctrine()->getManager();
        $page = intval($request->get('page') ?? 1);
        $ip   = strval($request->get('ip'));

        $log_viewer = new RequestLogViewer($em);
        $data       = $log_viewer->getData($page, $ip);

        return $this->render('admin/index.html.twig', [
            'document' => [
              'title' => 'Просмотр логов запросов'
            ],
            'ip'   => $ip,
            'page' => $page,
            'data' => $data
        ]);
    }
}
